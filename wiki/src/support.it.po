# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-05-24 06:37+0000\n"
"PO-Revision-Date: 2019-07-20 10:40+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Tails translators <tails-l10n@boum.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Support\"]]\n"
msgstr "[[!meta title=\"Supporto\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Search the documentation"
msgstr "Cercare la documentazione"

#. type: Plain text
msgid ""
"Read the [[official documentation|doc]] to learn more about how Tails works "
"and maybe start answering your questions. It contains:"
msgstr ""
"Leggi la  [[documetazione ufficiale|doc]] per conoscere più cose riguardo a "
"come lavora Tails e come potresti iniziare a rispondere alle tue domande. "
"Esso contiene :"

#. type: Bullet: '  - '
msgid "General information about what Tails is"
msgstr "Informazioni generali su cosa Tails sia"

#. type: Bullet: '  - '
msgid ""
"Information to understand how it can help you and what its limitations are"
msgstr ""
"Informazioni per capire come essa può aiutarti e quali sono i suoi limiti"

#. type: Bullet: '  - '
msgid "Guides covering typical uses of Tails"
msgstr "Guide che nascondono tipici usi di Tails"

#. type: Plain text
msgid "[[Visit Tails documentation|doc]]"
msgstr "[[Consulta la documentazione di Tails|doc]]"

#. type: Title =
#, no-wrap
msgid "Learn how to use Tails"
msgstr "Imparare ad usare Tails"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"support/learn/intro.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"support/learn/intro.inline.it\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title =
#, no-wrap
msgid "Frequently asked questions"
msgstr "Risposte a domande frequenti (F.A.Q)"

#. type: Plain text
msgid "Search our list of [[frequently asked questions|faq]]."
msgstr "Cerca la nostra lista di [[risposte a domande frequenti|faq]]."

#. type: Title =
#, no-wrap
msgid "Upgrade"
msgstr "Aggiornamento"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Make sure you are using the latest version, as [[upgrading|doc/upgrade]] "
#| "might solve your problem."
msgid ""
"Make sure you are using the latest version, as [[upgrading|doc/upgrade]] "
"might solve your problem."
msgstr ""
"Assicurati di usare l'ultima versione, questo [[aggiornamento|doc/upgrade]] "
"potrebbe risolvere i tuoi problemi."

#. type: Title =
#, no-wrap
msgid "Check if the problem is already known"
msgstr "Controlla se il problema è già noto"

#. type: Plain text
msgid "You can have a look at:"
msgstr "Dovresti dare uno sguardo a  :"

#. type: Bullet: '  - '
msgid "The [[list of known issues|support/known_issues]]"
msgstr "La [[lista dei problemi noti|support/known_issues]]"

#. type: Bullet: '  - '
msgid ""
"The [[!tails_gitlab groups/tails/-/milestones desc=\"list of things that "
"will be fixed or improved in the next release\"]]"
msgstr ""

#. type: Bullet: '  - '
#, fuzzy
#| msgid "The [[!tails_redmine desc=\"rest of our open tickets on Redmine\"]]"
msgid "The [[!tails_gitlab desc=\"rest of our open issues on GitLab\"]]"
msgstr ""
"Il resto [[!tails_redmine desc=\"dei nostri ticket aperti su Redmine\"]]"

#. type: Plain text
#, no-wrap
msgid "<div id=\"bugs\" class=\"blocks two-blocks\">\n"
msgstr "<div id=\"bugs\" class=\"blocks two-blocks\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Report an error</h1>\n"
msgstr "  <h1>Segnalare un errore</h1>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If you are facing an error in Tails, please follow the [[bug reporting\n"
"  guidelines|doc/first_steps/bug_reporting]].</p>\n"
msgstr ""
"  <p>Se hai trovato un errore in Tails, per favore segui ia [[segnalazione di un errore\n"
"  |doc/first_steps/bug_reporting]].</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If Tails does not start, please see our specific\n"
"  [[reporting guidelines|doc/first_steps/bug_reporting#does_not_start]].</p>\n"
msgstr ""
"  <p>Se Tails non si avvia, per favore guarda le nostre specifiche\n"
"[[linee guida per la segnalazione|doc/first_steps/bug_reporting/tails_does_not_start]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"wishlist\" class=\"blocks two-blocks\">\n"
msgstr "<div id=\"wishlist\" class=\"blocks two-blocks\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Request a feature</h1>\n"
msgstr "  <h1>Richiedere una funzionalità</h1>\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "  <p>If you would like to see a new feature in Tails,\n"
#| "  search the [[!tails_redmine desc=\"open tickets in Redmine\"]] first,\n"
#| "  and file a new ticket in there if no existing one matches your needs.</p>\n"
msgid ""
"  <p>If you would like to see a new feature in Tails,\n"
"  search the [[!tails_gitlab desc=\"open issues in GitLab\"]] first,\n"
"  and file a new issue on GitLab if no existing one matches your needs.</p>\n"
msgstr ""
"  <p>Se vuoi vedere un nuova funzionalità in Tails,\n"
"cerca in [[!tails_redmine desc=\"ticket aperti in Redmine\"]] prima,\n"
"e presenta un nuovo ticket se non esiste o se nessuno assomiglia a quello che ti serve.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #wishlist -->\n"
msgstr "</div> <!-- #wishlist -->\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"talk\">\n"
msgstr "<div id=\"talk\">\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "  <h1>Get in touch with us</h1>\n"
msgid "  <h1>Write to our help desk</h1>\n"
msgstr "  <h1>Contattaci</h1>\n"

#. type: Plain text
#, no-wrap
msgid "  [[!inline pages=\"support/talk\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "  [[!inline pages=\"support/talk.it\" raw=\"yes\" sort=\"age\"]]\n"

#, fuzzy
#~| msgid ""
#~| "The [list of things that will be in the next release](https://redmine."
#~| "tails.boum.org/code/projects/tails/issues?query_id=111)"
#~ msgid ""
#~ "The [list of things that will be in the next release](https://redmine."
#~ "tails.boum.org/code/projects/tails/issues?query_id=327)"
#~ msgstr ""
#~ "La [lista delle cose che verranno inserite nella prosssima release]"
#~ "(https://redmine.tails.boum.org/code/projects/tails/issues?query_id=111)"

#~ msgid "<div id=\"page-found_a_problem\">\n"
#~ msgstr "<div id=\"page-found_a_problem\">\n"

#~ msgid "<div id=\"bugs\">\n"
#~ msgstr "<div id=\"bugs\">\n"

#~ msgid "</div> <!-- #bugs -->\n"
#~ msgstr "</div> <!-- #bugs -->\n"

#~ msgid "</div> <!-- #page-found_a_problem -->\n"
#~ msgstr "</div> <!-- #page-found_a_problem -->\n"

#~ msgid "</div> <!-- #talk -->\n"
#~ msgstr "</div> <!-- #talk -->\n"

#, fuzzy
#~ msgid ""
#~ "The [rest of our open tickets on Redmine](https://redmine.tails.boum.org/"
#~ "code/projects/tails/issues?set_filter=1)"
#~ msgstr ""
#~ "La [lista delle cose che verranno inserite nella prosssima release]"
#~ "(https://redmine.tails.boum.org/code/projects/tails/issues?query_id=111)"

#~ msgid "The [[list of things to do|todo]]"
#~ msgstr "La [[lista delle cose da fare|todo]]"

#~ msgid "</div> <!-- .container -->\n"
#~ msgstr "</div> <!-- .container -->\n"

#~ msgid "</div> <!-- #support -->\n"
#~ msgstr "</div> <!-- #support -->\n"

#~ msgid "How-tos on getting Tails to work"
#~ msgstr "Des tutoriels pour faire fonctionner Tails"

#~ msgid "It contains:"
#~ msgstr "Elle contient :"

#~ msgid "Troubleshooting"
#~ msgstr "Comprendre un problème"

#~ msgid ""
#~ "If you have found an error in Tails or if you would like to see a new "
#~ "feature in it, have a look at the [[support/troubleshooting]] page."
#~ msgstr ""
#~ "Si vous avez trouvé une erreur dans Tails ou si vous voulez y voir une "
#~ "nouvelle fonctionnalité, allez voir la page [[comprendre un problème|"
#~ "support/troubleshooting]] page."

#~ msgid "Found a problem?"
#~ msgstr "Un problème ?"
