[[!meta title="Logging in to captive portals"]]

[[!inline pages="doc/anonymous_internet/unsafe_browser/captive_portal.inline" raw="yes" sort="age"]]

Tor cannot be started when your Internet connection is initially blocked
by a captive portal. So, Tails includes an
<span class="application">Unsafe Browser</span> to log in to captive
portals before starting Tor.

To start the <span class="application">Unsafe Browser</span>, choose
<span class="menuchoice">
  <span class="guimenu">Applications</span>&nbsp;▸
  <span class="guisubmenu">Internet</span>&nbsp;▸
  <span class="guimenuitem">Unsafe Web Browser</span></span>.

The <span class="application">Unsafe Browser</span> has a red theme to
differentiate it from [[<span class="application">Tor Browser</span>|Tor_Browser]].

<div class="caution">

<p><strong>The <span class="application">Unsafe Browser</span> is not
anonymous</strong>. Use it only to log in to captive portals or to
[[browse web pages on the local network|advanced_topics/lan#browser]].</p>

</div>

<div class="note">

[[!inline pages="doc/anonymous_internet/unsafe_browser/chroot.inline" raw="yes" sort="age"]]

</div>

Security recommendations:

* Do not run this browser at the same time as the anonymous
  [[<span class="application">Tor Browser</span>|Tor_Browser]]. This makes it easy to not mistake one browser for the
  other, which could have catastrophic consequences.
