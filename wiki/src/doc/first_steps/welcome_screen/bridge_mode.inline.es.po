# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-03-25 05:24+0000\n"
"PO-Revision-Date: 2019-08-24 06:21+0000\n"
"Last-Translator: Joaquín Serna <bubuanabelas@cryptolab.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"bridge_modeinline/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "When <span class=\"application\">Tails Greeter</span> appears, click on "
#| "the <span class=\"button\">[[!img lib/list-add.png alt=\"Expand\" class="
#| "\"symbolic\" link=\"no\"]]</span> button."
msgid ""
"When the Welcome Screen appears, click on the <span class=\"button\">[[!img "
"lib/list-add.png alt=\"Expand\" class=\"symbolic\" link=\"no\"]]</span> "
"button."
msgstr ""
"Cuando aparezca el <span class=\"application\">Tails Greeter</span>, haz "
"click en el botón <span class=\"button\">[[!img lib/list-add.png alt="
"\"Expandir\" class=\"symbolic\" link=\"no\"]]</span>."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   [[!img additional.png link=\"no\" alt=\"Additional settings of Tails Greeter\"]]\n"
msgid "   [[!img additional.png link=\"no\" alt=\"Additional settings of the Welcome Screen\"]]\n"
msgstr "   [[!img additional.png link=\"no\" alt=\"Configuraciones adicionales de Tails Greeter\"]]\n"

#. type: Bullet: '2. '
msgid ""
"When the <span class=\"guilabel\">Additional Settings</span> dialog appears, "
"click on <span class=\"guilabel\">Network Configuration</span>."
msgstr ""
"Cuando aparezca el diálogo <span class=\"guilabel\">Configuraciones "
"adicionales</span>, haz click en <span class=\"guilabel\">Configuración de "
"Red</span>."

#. type: Bullet: '3. '
msgid ""
"Select the <span class=\"guilabel\">Configure a Tor bridge or local proxy</"
"span> option."
msgstr ""
"Selecciona la opción <span class=\"guilabel\">Configura un puente Tor o un "
"proxy local</span>."

#. type: Plain text
msgid ""
"Then, after starting the working session and connecting to the network, an "
"assistant will guide you through the configuration of Tor."
msgstr ""
"Después de iniciar la sesión y conectarse a la red, un asistente te guiará "
"para configurar Tor."
