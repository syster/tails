# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: 2019-09-21 12:20+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Encrypted persistence\"]]\n"
msgid "[[!meta title=\"Encrypted Persistent Storage\"]]\n"
msgstr "[[!meta title=\"Persistance chiffrée\"]]\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "If you start Tails from a USB stick, you can create a persistent volume "
#| "in the free space left on the USB stick.  The files in the persistent "
#| "volume are saved encrypted and remain available across separate working "
#| "sessions."
msgid ""
"If you start Tails from a USB stick, you can create a Persistent Storage in "
"the free space left on the USB stick.  The files and settings stored in the "
"Persistent Storage are saved encrypted and remain available across different "
"working sessions."
msgstr ""
"Si vous avez démarré Tails depuis une clé USB, vous pouvez créer un volume "
"persistant sur l'espace libre de la clé. Les données contenues dans le "
"volume persistant sont sauvegardées de manière chiffrée et restent "
"disponibles d'une session de travail à l'autre."

#. type: Plain text
#, fuzzy
#| msgid "You can use this persistent volume to store any of the following:"
msgid "You can use this Persistent Storage to store, for example:"
msgstr "Vous pouvez utiliser le volume persistant pour stocker :"

#. type: Bullet: '  - '
msgid "Personal files"
msgstr "Des fichiers personnels"

#. type: Bullet: '  - '
msgid "Some settings"
msgstr "Certains réglages"

#. type: Bullet: '  - '
msgid "Additional software"
msgstr "Des logiciels additionnels"

#. type: Bullet: '  - '
msgid "Encryption keys"
msgstr "Des clés de chiffrement"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "The persistent volume is an encrypted partition protected by a passphrase."
msgid ""
"The Persistent Storage is an encrypted partition protected by a passphrase "
"on the USB stick."
msgstr ""
"Le volume persistant est une partition chiffrée protégée par une phrase de "
"passe."

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Once the persistent volume is created, you can choose to activate it or "
#| "not each time you start Tails."
msgid ""
"After you create a Persistent Storage, you can choose to unlock it or not "
"each time you start Tails."
msgstr ""
"Une fois le volume persistant créé, vous pouvez choisir de l'activer, ou "
"non, à chaque démarrage de Tails."

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "You can use this persistent volume to store any of the following:"
msgid "How to use the Persistent Storage"
msgstr "Vous pouvez utiliser le volume persistant pour stocker :"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Warnings about persistence|first_steps/persistence/warnings]]"
msgid ""
"[[Warnings about the Persistent Storage|first_steps/persistence/warnings]]"
msgstr ""
"[[Avertissement à propos de la persistance|first_steps/persistence/warnings]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid ""
#| "[[Create & configure the persistent volume|first_steps/persistence/"
#| "configure]]"
msgid ""
"[[Creating and configuring the Persistent Storage|first_steps/persistence/"
"configure]]"
msgstr ""
"[[Créer et configurer le volume persistant|first_steps/persistence/"
"configure]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Enable & use the persistent volume|first_steps/persistence/use]]"
msgid ""
"[[Unlocking and using the Persistent Storage|first_steps/persistence/use]]"
msgstr ""
"[[Activer et utiliser le volume persistant|first_steps/persistence/use]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid ""
#| "[[Manually copy your persistent data to a new USB stick|first_steps/"
#| "persistence/copy]]"
msgid ""
"[[Making a backup of your Persistent Storage|first_steps/persistence/copy]]"
msgstr ""
"[[Copier manuellement vos données persistantes vers une nouvelle clé USB|"
"first_steps/persistence/copy]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Delete the persistent volume|first_steps/persistence/delete]]"
msgid "[[Deleting the Persistent Storage|first_steps/persistence/delete]]"
msgstr "[[Supprimer le volume persistant|first_steps/persistence/delete]]"

#~ msgid ""
#~ "How to use the persistent volume\n"
#~ "=================================\n"
#~ msgstr ""
#~ "Comment utiliser le volume persistant\n"
#~ "=================================\n"

#~ msgid ""
#~ "[[Change the passphrase of the persistent volume|first_steps/persistence/"
#~ "change_passphrase]]"
#~ msgstr ""
#~ "[[Changer la phrase de passe du volume persistant|first_steps/persistence/"
#~ "change_passphrase]]"

#~ msgid ""
#~ "[[Check the file system of the persistent volume|first_steps/persistence/"
#~ "check_file_system]]"
#~ msgstr ""
#~ "[[Vérifier le système de fichiers du volume persistant|first_steps/"
#~ "persistence/check_file_system]]"

#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#~ msgid ""
#~ "<p>It is only possible to create a persistent volume if the USB stick\n"
#~ "was installed using <span class=\"application\">Tails Installer</span>.</"
#~ "p>\n"
#~ msgstr ""
#~ "<p>Il est uniquement possible de créer un volume persistant si la clé "
#~ "USB\n"
#~ "a été installé via l'<span class=\"application\">Installeur de Tails</"
#~ "span>.</p>\n"

#~ msgid ""
#~ "<p>This requires a USB stick of <strong>at least 8 GB</strong>.</p>\n"
#~ msgstr ""
#~ "<p>Cela requiert une clé USB d'<strong>au moins 8 Go</strong>.</p>\n"

#~ msgid "</div>\n"
#~ msgstr "</div>\n"

#~ msgid "your personal files and working documents"
#~ msgstr "vos données personnelles et vos documents de travail"

#~ msgid "the software packages that you download and install in Tails"
#~ msgstr "les paquets logiciels que vous téléchargez et installez dans Tails"

#~ msgid "the configuration of the programs you use"
#~ msgstr "la configuration des logiciels que vous utilisez"
