# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: 2018-07-02 07:15+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"virt-manager\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"[*virt-manager*](http://virt-manager.org/) is a free software virtualization "
"solution for Linux. *virt-manager* has a more complex interface than "
"*VirtualBox* or *GNOME Boxes* but it also has a more complete set of "
"features."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/advanced_topics/virtualization.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/advanced_topics/virtualization.caution.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p><em>virt-manager</em> is the only virtualization\n"
"solution that we present that allows the use of a Persistent\n"
"Storage.</span> See [[Running Tails from a USB\n"
"image|virt-manager#usb_image]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>The following instructions have been tested on Debian 9 (Stretch).</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>Please, [[let us know|about/contact#tails-dev]] if they do not apply\n"
"to Debian 10 (Buster).</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Terminology"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"*virt-manager* is based on a set of lower level virtualization tools,\n"
"going from the user interface to the hardware interactions with the\n"
"processor. This terminology is a bit confusing and other documentation\n"
"might mention the following tools:\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"*KVM* is the module of the Linux kernel that interacts with the "
"virtualization features of the processor."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"*QEMU* is the virtualization software that emulates virtual processors and "
"peripherals based on *KVM* and that starts and stops virtual machines."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"*libvirt* is a library that allows *virt-manager* to interact with the "
"virtualization capabilities provided by *QEMU*."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"*SPICE* is a protocol that allows to visualize the desktop of virtual "
"machines."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"*virt-manager* is the graphical interface that allows to create, configure, "
"and run virtual machines."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"iso\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Installation"
msgstr ""

#. type: Plain text
msgid "To install *virt-manager* in Debian, execute the following command:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    sudo apt install virt-manager libvirt-daemon-system\n"
msgstr ""

#. type: Plain text
msgid "To install *virt-manager* in Ubuntu, execute the following command:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    sudo apt install virt-manager libvirt-bin qemu-kvm\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Running Tails from an ISO image"
msgstr ""

#. type: Bullet: '1. '
msgid "Start *virt-manager*."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Double-click on **localhost (QEMU)** to connect to the *QEMU* system of your "
"host."
msgstr ""
