# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-05-22 15:30+0000\n"
"PO-Revision-Date: 2020-01-12 13:29+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Documentation\"]]\n"
msgstr "[[!meta title=\"Documentation\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If this section doesn't answer your questions, you can also look at our\n"
"[[FAQ|support/faq]].</p>\n"
msgstr ""
"<p>Si cette section ne répond pas à vos questions, vous pouvez également\n"
"consulter notre [[foire aux questions (FAQ)|support/faq]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"Read about how you can help [[improving Tails documentation|/contribute/how/"
"documentation]]."
msgstr ""
"Si vous souhaitez aider, lisez comment [[améliorer la documentation de "
"Tails|/contribute/how/documentation]]."

#. type: Plain text
msgid "- [[Introduction to this documentation|introduction]]"
msgstr "- [[Introduction sur cette documentation|introduction]]"

#. type: Title #
#, no-wrap
msgid "General information"
msgstr "Informations générales"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/about.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/about.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"install\"></a>\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Download, installation, and upgrade"
msgstr "Téléchargement, installation et mise à jour"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "  - [[!traillink Upgrading_a_Tails_USB_stick|upgrade]]\n"
#| "    - [[Manually_upgrading_from_your_Tails|upgrade/tails-overview]]\n"
#| "    - [[Manually_upgrading_from_Windows|upgrade/win-overview]]\n"
#| "    - [[Manually_upgrading_from_macOS|upgrade/mac-overview]]\n"
#| "    - [[Manually_upgrading_from_Linux|upgrade/linux-overview]]\n"
#| "    - [[Manually_upgrading_by_cloning_from_another_Tails|upgrade/clone-overview]]\n"
#| "    - [[!traillink Repairing_a_Tails_USB_stick_that_fails_to_start_after_an_upgrade|upgrade/repair]]\n"
#| "  - Uninstalling Tails or resetting a USB stick using\n"
#| "    [[!traillink Linux|reset/linux]],\n"
#| "    [[!traillink Windows|reset/windows]], or\n"
msgid ""
"  - Downloading without installing:\n"
"    - [[For USB sticks (USB image)|install/download]]\n"
"    - [[For DVDs (ISO image)|install/dvd-download]]\n"
"    - [[For virtual machines (ISO image)|install/vm-download]]\n"
"  - [[Installing from another Tails (for PC)|install/win/clone-overview]]\n"
"  - [[Installing from another Tails (for Mac)|install/mac/clone-overview]]\n"
"  - [[Installing from Windows|install/win/usb-overview]]\n"
"  - [[Installing from macOS|install/mac/usb-overview]]\n"
"  - [[Installing from Linux (recommended)|install/linux/usb-overview]]\n"
"  - [[Installing from Debian, Ubuntu, or Mint using the command line and GnuPG (experts)|install/expert/usb-overview]]\n"
"  - [[Burning a DVD|install/dvd]]\n"
"  - [[!traillink Upgrading_a_Tails_USB_stick|upgrade]]\n"
"    - [[Manually_upgrading_from_your_Tails|upgrade/tails-overview]]\n"
"    - [[Manually_upgrading_from_Windows|upgrade/win-overview]]\n"
"    - [[Manually_upgrading_from_macOS|upgrade/mac-overview]]\n"
"    - [[Manually_upgrading_from_Linux|upgrade/linux-overview]]\n"
"    - [[Manually_upgrading_by_cloning_from_another_Tails|upgrade/clone-overview]]\n"
"    - [[!traillink Repairing_a_Tails_USB_stick_that_fails_to_start_after_an_upgrade|upgrade/repair]]\n"
"  - Uninstalling Tails or resetting a USB stick using\n"
"    [[!traillink Linux|reset/linux]],\n"
"    [[!traillink Windows|reset/windows]], or\n"
msgstr ""
"  - [[!traillink Mettre_à_jour_une_clé_USB_Tails|upgrade]]\n"
"    - [[Mettre_à_jour_manuellement_depuis_votre_Tails|upgrade/tails-overview]]\n"
"    - [[Mettre_à_jour_manuellement_depuis_Windows|upgrade/win-overview]]\n"
"    - [[Mettre_à_jour_manuellement_depuis_macOS|upgrade/mac-overview]]\n"
"    - [[Mettre_à_jour_manuellement_depuis_Linux|upgrade/linux-overview]]\n"
"    - [[Mettre_à_jour_manuellement_en_clonant_ depuis_un_autre_Tails|upgrade/clone-overview]]\n"
"    - [[!traillink Réparer_une_clé_USB_Tails_qui_n'arrive_pas_à_démarrer_après_une_mise_à_jour|upgrade/repair]]\n"
"  - Désinstaller Tails ou réinitialiser une clé USB avec\n"
"    [[!traillink Linux|reset/linux]],\n"
"    [[!traillink Windows|reset/windows]] ou\n"

#. type: Plain text
#, no-wrap
msgid "    [[!traillink Mac|reset/mac]]\n"
msgstr "    [[!traillink Mac|reset/mac]]\n"

#. type: Title #
#, no-wrap
msgid "First steps with Tails"
msgstr "Premiers pas avec Tails"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Anonymous Internet"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Encryption and privacy"
msgstr "Chiffrement et vie privée"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/encryption_and_privacy.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Work on sensitive documents"
msgstr "Travailler sur des documents sensibles"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title #
#, no-wrap
msgid "Advanced topics"
msgstr "Sujets avancés"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/advanced_topics.index\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/advanced_topics.index.fr\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "[[Downloading without installing|install/download]]"
#~ msgstr "[[Télécharger sans installer|install/download]]"

#~ msgid ""
#~ "[[Installing from another Tails (for PC)|install/win/clone-overview]]"
#~ msgstr ""
#~ "[[Installation depuis un autre Tails (PC)|install/win/clone-overview]]"

#~ msgid ""
#~ "[[Installing from another Tails (for Mac)|install/mac/clone-overview]]"
#~ msgstr ""
#~ "[[Installation depuis un autre Tails (Mac)|install/mac/clone-overview]]"

#~ msgid "[[Installing from Windows|install/win/usb-overview]]"
#~ msgstr "[[Installation depuis Windows|install/win/usb-overview]]"

#~ msgid "[[Installing from macOS|install/mac/usb-overview]]"
#~ msgstr "[[Installation depuis macOS|install/mac/usb-overview]]"

#~ msgid "[[Installing from Linux (recommended)|install/linux/usb-overview]]"
#~ msgstr ""
#~ "[[Installation depuis Linux (recommandé)|install/linux/usb-overview]]"

#~ msgid ""
#~ "[[Installing from Debian, Ubuntu, or Mint using the command line and "
#~ "GnuPG (experts)|install/expert/usb-overview]]"
#~ msgstr ""
#~ "[[Installation depuis Debian, Ubuntu ou Mint avec la ligne de commande et "
#~ "GnuPG (experts)|install/expert/usb-overview]]"

#~ msgid "[[Burning a DVD|install/dvd]]"
#~ msgstr "[[Graver un DVD|install/dvd]]"

#~ msgid "Connect to the Internet anonymously"
#~ msgstr "Se connecter à internet anonymement"

#~ msgid "Download and install"
#~ msgstr "Téléchargement et installation"

#~ msgid "[[Install from Debian, Ubuntu, or Mint|install/debian/usb-overview]]"
#~ msgstr ""
#~ "[[Installation à partir de Debian, Ubuntu ou Mint|install/debian/usb-"
#~ "overview]]"

#~ msgid ""
#~ "[[Install from other Linux distributions|install/linux/usb-overview]]"
#~ msgstr ""
#~ "[[Installation à partir d'une autre distribution Linux|install/linux/usb-"
#~ "overview]]"

#~ msgid ""
#~ "[[Install from macOS by burning a DVD first|install/mac/dvd-overview]]"
#~ msgstr ""
#~ "[[Installation à partir de macOS en gravant d'abord un DVD|install/mac/"
#~ "dvd-overview]]"

#~ msgid "[[Download and verify using OpenPGP|install/download/openpgp]]"
#~ msgstr ""
#~ "[[Télécharger et vérifier en utilisant OpenPGP|install/download/openpgp]]"

#~ msgid "This documentation is a work in progress and a collective task."
#~ msgstr ""
#~ "Cette documentation est en perpétuelle évolution, ainsi qu'un travail "
#~ "collectif."

#, fuzzy
#~| msgid ""
#~| "  - [[Verify the ISO image|doc/get/verify]]\n"
#~| "    - [[Using Gnome: Ubuntu, Debian, Tails, etc.|get/"
#~| "verify_the_iso_image_using_gnome]]\n"
#~| "    - [[Using Linux with the command line|get/"
#~| "verify_the_iso_image_using_the_command_line]]\n"
#~| "    - [[Using other operating systems|get/"
#~| "verify_the_iso_image_using_other_operating_systems]]\n"
#~| "    - [[Trusting Tails signing key|get/trusting_tails_signing_key]]\n"
#~ msgid ""
#~ "  - [[Verify the ISO image|doc/get/verify]]\n"
#~ "    - [[Using Gnome: Ubuntu, Debian, Tails, etc.|get/"
#~ "verify_the_iso_image_using_gnome]]\n"
#~ "    - [[Using Linux with the command line|get/"
#~ "verify_the_iso_image_using_the_command_line]]\n"
#~ "    - [[Using other operating systems|get/"
#~ "verify_the_iso_image_using_other_operating_systems]]\n"
#~ "    - [[Trusting Tails signing key|get/trusting_tails_signing_key]]\n"
#~ "  - [[Download and verify using OpenPGP|install/download/openpgp]]\n"
#~ msgstr ""
#~ "  - [[Vérifier l'image ISO|doc/get/verify]]\n"
#~ "    - [[Avec Gnome : Ubuntu, Debian, Tails, etc.|get/"
#~ "verify_the_iso_image_using_gnome]]\n"
#~ "    - [[Avec Linux et la ligne de commande|get/"
#~ "verify_the_iso_image_using_the_command_line]]\n"
#~ "    - [[Avec un autre système d'exploitation|get/"
#~ "verify_the_iso_image_using_other_operating_systems]]\n"
#~ "    - [[Faire confiance à la clé de signature de Tails|get/"
#~ "trusting_tails_signing_key]]\n"

#~ msgid "[[!inline pages=\"doc/get.index\" raw=\"yes\"]]\n"
#~ msgstr "[[!inline pages=\"doc/get.index.fr\" raw=\"yes\"]]\n"

#~ msgid "[[Encryption & privacy|encryption_and_privacy]]"
#~ msgstr "[[Chiffrement et vie privée|encryption_and_privacy]]"

#~ msgid ""
#~ "This documentation is a work in progress and a collective task. If you "
#~ "think it lacks documentation on a specific topic you can suggest us to "
#~ "complete it or try to write it yourself and share it with us."
#~ msgstr ""
#~ "Cette documentation est en perpétuelle évolution ainsi qu'un travail "
#~ "collectif. Si vous pensez qu'il y a des manques sur un sujet spécifique, "
#~ "vous pouvez nous suggérer de la compléter ou essayer de le faire par vous-"
#~ "même et partager le résultat avec nous."
