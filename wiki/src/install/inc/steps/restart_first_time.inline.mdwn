<div class="trophy windows linux mac expert">

<div class="state-image">[[!img install/inc/infography/tails-usb.png link="no" alt="Tails USB stick"]]</div>

<p>Congratulations, you have installed Tails on your USB stick!</p>

<p>You will now restart your computer on this USB stick.
<span class="windows linux expert">It can be a bit complicated, so good luck!</span>
<span class="mac">But it might not work on your Mac model, so good luck!</span>
</p>

</div>

<h1 id="back">Open these instructions on another device</h1>

<div class="step-image">[[!img install/inc/infography/switch-context.png link="no" alt=""]]</div>

In the next step, you will shut down the computer. To be able to follow
the rest of the instructions afterwards, we recommend you either:

   [[!img install/inc/qrcodes/tails_boum_org_install_clone_back_1.png class="install-clone qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_install_win_usb_back_1.png class="windows qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_install_mac_usb_back_1.png class="mac qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_install_mac_clone_back_1.png class="mac-clone qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_install_expert_usb_back_1.png class="expert qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_install_linux_usb_back_1.png class="linux qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_upgrade_clone_back_1.png class="upgrade-clone qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_upgrade_tails_back_1.png class="upgrade-tails qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_upgrade_win_back_1.png class="upgrade-windows qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_upgrade_mac_back_1.png class="upgrade-mac qrcode" link="no" alt=""]]
   [[!img install/inc/qrcodes/tails_boum_org_upgrade_linux_back_1.png class="upgrade-linux qrcode" link="no" alt=""]]

   - Open this page on your smartphone, tablet, or another computer (recommended).

   - Print the rest of the instructions on paper.

   - Take note of the URL of this page to be able to come back later:

     <span class="install-clone">`https://tails.boum.org/install/clone?back=1`</span>
     <span class="windows">`https://tails.boum.org/install/win/usb?back=1`</span>
     <span class="mac">`https://tails.boum.org/install/mac/usb?back=1`</span>
     <span class="mac-clone">`https://tails.boum.org/install/mac/clone?back=1`</span>
     <span class="expert">`https://tails.boum.org/install/expert/usb?back=1`</span>
     <span class="linux">`https://tails.boum.org/install/linux/usb?back=1`</span>
     <span class="upgrade-clone">`https://tails.boum.org/upgrade/clone?back=1`</span>
     <span class="upgrade-tails">`https://tails.boum.org/upgrade/tails?back=1`</span>
     <span class="upgrade-windows">`https://tails.boum.org/upgrade/win?back=1`</span>
     <span class="upgrade-mac">`https://tails.boum.org/upgrade/mac?back=1`</span>
     <span class="upgrade-linux">`https://tails.boum.org/upgrade/linux?back=1`</span>

<h1 id="start-intermediary" class="upgrade-tails upgrade-os">Restart on the intermediary Tails</h1>
<h1 id="start-other" class="clone">Restart on the other Tails</h1>
<h1 id="start-tails" class="windows linux mac expert">Restart on Tails</h1>

<div class="step-image">
[[!img install/inc/infography/plug-other-tails.png link="no" class="clone" alt="USB stick plugged on the left"]]
[[!img install/inc/infography/restart-on-tails.png link="no" class="windows linux mac" alt="Computer restarted on USB stick"]]
[[!img install/inc/infography/restart-on-other-tails.png link="no" class="clone upgrade-os" alt="Computer restarted on USB stick on the left"]]
[[!img install/inc/infography/restart-on-upgrade-usb.png link="no" class="upgrade-tails" alt="USB stick unplugged on the right and computer restarted on USB stick on the left"]]
</div>

<div class="windows linux expert install-clone upgrade-clone upgrade-tails upgrade-windows upgrade-linux">
[[!inline pages="install/inc/steps/pc_boot_menu.inline" raw="yes" sort="age"]]
</div>

<div class="mac mac-clone upgrade-mac">
[[!inline pages="install/inc/steps/mac_startup_disks.inline" raw="yes" sort="age"]]
</div>

<a id="greeter"></a>

<h2 id="welcome-screen">Welcome Screen</h2>

1. One to two minutes after the Boot Loader, the Welcome Screen appears.

   <div class="note">
   <p>If the computer display the error message
   <span class="guilabel">Error starting GDM with your graphics
   card</span>, refer to our [[list of known issues with graphics
   cards|support/known_issues/graphics]].</p>

   <p>If the computer stops responding or displays other error messages before
   getting to the Welcome Screen, refer to
   [[!toggle id="not_entirely" text="the troubleshooting section about
   Tails not starting entirely"]].</p>
   </div>

   [[!toggleable id="not_entirely" text="""
   <span class="hide">[[!toggle id="not_entirely" text=""]]</span>

   Troubleshooting
   ---------------

   Tails does not start entirely
   -----------------------------

   The following section applies if the Boot Loader appears but not
   the Welcome Screen when starting Tails.

   1. Restart the computer on the first USB stick.

   1. In the Boot Loader, choose the
   <span class="guilabel">Troubleshooting Mode</span>
   which might work better on some computers.

   1. If the <span class="guilabel">Troubleshooting Mode</span> does not work
   better, restart the computer again on the first USB stick.

   1. Depending on whether the Boot Loader is <a href="#GRUB">*GRUB*</a> or <a href="#SYSLINUX">*SYSLINUX*</a>:

      <a id="GRUB">If the Boot Loader is *GRUB*:</a>
        1. Press **e** when GRUB appears.

           A new screen appears with more options.

           <img src="file:///home/amnesia/Persistent/Tor%20Browser/tails/doc/advanced_topics/boot_options/grub-with-options.png" width="540" alt="GRUB with a list of options starting with 'setparams Tails'"/>

        1. Navigate with the arrows of the keyboard to the end of the line that
           starts with <span class="code">linux</span>. The line is most likely
           wrapped and displayed on several lines on the screen but it is a
           single configuration line.

        1. Press the <span class="keycap">Backspace</span> key several times to
           remove the <span class="code">quiet</span> option from the command line.

        1. Type <span class="code">debug nosplash</span> to add these options to the command line.

        1. If Tails previously failed to start and displayed the
           following error:

           <p class="pre">Probing EDD (edd=off to disable)...</span>

           Then type `edd=off` to add this option to the command line.

        1. Press **Ctrl+X** to start Tails.

        1. Hopefully, new error messages now appear when starting Tails. You can
           [[send them to our help desk by email|support/talk]], for example by taking a
           picture of the last screen and error messages.

      <a id="SYSLINUX">If the Boot Loader is *SYSLINUX*:</a> 
        1. Press
           <span class="keycap">Tab</span>. A command line with a list
           of boot options appears at the bottom of the screen.

           <img src="file:///home/amnesia/Persistent/Tor%20Browser/tails/doc/advanced_topics/boot_options/syslinux-with-options.png" width="540" alt="SYSLINUX with a list of options starting with '/live/vmlinuz' at the bottom"/>

        1. Press the <span class="keycap">Backspace</span> key several times to
           remove the <span class="code">quiet</span> option from the command line.

        1. Type <span class="code">debug nosplash</span> to add these options to the command line.

        1. If Tails previously failed to start and displayed the
           following error:

           <p class="pre">Probing EDD (edd=off to disable)...</span>

           Then type <span class="code">edd=off</span> to add this option to the command line.

        1. Press <span class="keycap">Enter</span> to start Tails.

        1. Hopefully, new error messages now appear when starting Tails. You can
           [[send them to our help desk by email|support/talk]], for example by taking a
           picture of the last screen and error messages.

   """]]

   [[!img doc/first_steps/welcome_screen/welcome-screen.png link="no" alt="Welcome to Tails!"]]

1. In the Welcome Screen, select your language and
keyboard layout in the <span class="guilabel">Language & Region</span> section.
Click <span class="button">Start Tails</span>.

1. After 15&ndash;30 seconds, the Tails desktop appears.

   [[!img install/inc/screenshots/desktop.png link="no" alt="Tails desktop"]]