[[!meta title="Calendar"]]

All times are referenced to Berlin and Paris time.

# 2020 Q2

* 2020-06-02: **Release 4.7** (Firefox 68.9, kibi is the RM)

* 2020-06-03, 16:00: Foundations Team meeting

* 2020-06-06 to 2020-06-09: sysadmin team sprint

* 2020-06-30: **Release 4.8** (Firefox 68.10)

# 2020 Q3

* 2020-07-06, 16:00: Foundations Team meeting

* 2020-07-28: **Release 4.9** (Firefox 68.11)

* 2020-08-03, 16:00: Foundations Team meeting

* 2020-08-25: **Release 4.10** (Firefox 68.12)

* 2020-09-03, 16:00: Foundations Team meeting

* 2020-09-22: **Release 4.11** (Firefox 78.3, major release -- probably)

# 2020 Q4

* 2020-10-06, 16:00: Foundations Team meeting

* 2020-11-03, 16:00: Foundations Team meeting

* 2020-12-03, 16:00: Foundations Team meeting
